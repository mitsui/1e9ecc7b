#!/bin/bash

set -e

export HOME=`pwd`
DEPS_VER="001"
TARGET_HOST=${TARGET_HOST:-x86_64-linux-gnu}
BUILD_DIRECTORY=$HOME/build_dir
PREFIX_LIBS=$BUILD_DIRECTORY/deps
ARIA2_PREFIX=$HOME/aria2_build
LIBUV_OPTION=""

[ -f $PREFIX_LIBS/DEPMARK_$DEPS_VER ] || rm -rf $PREFIX_LIBS || true

DOWNLOADER="wget -c"
NUM_THREAD=-j`nproc`

## DEPENDENCES ##
ZLIB_URL=https://zlib.net/zlib-1.2.11.tar.xz
OPENSSL_URL=https://www.openssl.org/source/openssl-1.1.1d.tar.gz
LIBEXPAT_URL=https://github.com/libexpat/libexpat/releases/download/R_2_2_9/expat-2.2.9.tar.xz
SQLITE3_URL=https://www.sqlite.org/2019/sqlite-autoconf-3300100.tar.gz
C_ARES_URL=https://c-ares.haxx.se/download/c-ares-1.15.0.tar.gz
LIBSSH2_URL=https://www.libssh2.org/download/libssh2-1.9.0.tar.gz
LIBUV_URL=https://dist.libuv.org/dist/v1.34.0/libuv-v1.34.0.tar.gz

export CFLAGS=-O3
export CXXFLAGS=-O3
if [ "$TARGET_HOST" = "i386-linux-gnu" ]; then
  TARGET_HOST=x86_64-linux-gnu
  I386=yes
  export CFLAGS="$CFLAGS -m32"
  export CXXFLAGS="$CXXFLAGS -m32"
  export LDFLAGS="$LDFLAGS -m32"
fi
export PKG_CONFIG_PATH=$PREFIX_LIBS/lib/pkgconfig
export LD_LIBRARY_PATH=$PREFIX_LIBS/lib
export CC=${CC:-$TARGET_HOST-gcc}
export CXX=${CXX:-$TARGET_HOST-g++}
export AR=${AR:-$TARGET_HOST-ar}
export RANLIB=${RANLIB:-$TARGET_HOST-ranlib}

mkdir -p $BUILD_DIRECTORY || true
mkdir -p $PREFIX_LIBS || true
mkdir -p $ARIA2_PREFIX || true

build_zlib() {
  cd $BUILD_DIRECTORY
  rm -rf zlib-*
  $DOWNLOADER $ZLIB_URL
  tar xf zlib-*
  cd zlib-*/
  ./configure --prefix=$PREFIX_LIBS --static
  make $NUM_THREAD
  make install
}

build_expat() {
  cd $BUILD_DIRECTORY
  rm -rf expat-*
  $DOWNLOADER $LIBEXPAT_URL
  tar xf expat-*
  cd expat-*/
  ./configure --prefix=$PREFIX_LIBS --host=$TARGET_HOST --enable-static --disable-shared
  make $NUM_THREAD
  make install
}

build_c_ares() {
  cd $BUILD_DIRECTORY
  rm -rf c-ares-*
  $DOWNLOADER $C_ARES_URL
  tar xf c-ares-*
  cd c-ares-*/
  ./configure --prefix=$PREFIX_LIBS --host=$TARGET_HOST --enable-static --disable-shared
  make $NUM_THREAD
  make install
}

build_openssl() {
  cd $BUILD_DIRECTORY
  rm -rf openssl-*
  $DOWNLOADER $OPENSSL_URL
  tar xf openssl-*
  cd openssl-*/
  case "$TARGET_HOST" in
    x86_64-linux-gnu)
      if [ "$I386" = "yes" ]; then
        ./Configure --prefix=$PREFIX_LIBS linux-elf
      else
        ./Configure --prefix=$PREFIX_LIBS linux-x86_64
      fi
      ;;
    i686-w64-mingw32)
      CC=gcc CXX=g++ AR=ar RANLIB=ranlib ./Configure --cross-compile-prefix=$TARGET_HOST- --prefix=$PREFIX_LIBS mingw
      ;;
    x86_64-w64-mingw32)
      CC=gcc CXX=g++ AR=ar RANLIB=ranlib ./Configure --cross-compile-prefix=$TARGET_HOST- --prefix=$PREFIX_LIBS mingw64
      ;;
    arm-linux-gnueabihf)
      CC=gcc CXX=g++ AR=ar RANLIB=ranlib ./Configure --cross-compile-prefix=$TARGET_HOST- --prefix=$PREFIX_LIBS linux-armv4
      ;;
    *)
      echo "unknown target host"
      exit 1
      ;;
  esac
  make $NUM_THREAD
  make install_sw
}

build_sqlite3() {
  cd $BUILD_DIRECTORY
  rm -rf sqlite-autoconf-*
  $DOWNLOADER $SQLITE3_URL
  tar xf sqlite-autoconf-*
  cd sqlite-autoconf-*/
  ./configure --prefix=$PREFIX_LIBS --host=$TARGET_HOST --enable-static --disable-shared
  make $NUM_THREAD
  make install
}

build_libssh2() {
  cd $BUILD_DIRECTORY
  rm -rf libssh2-*
  $DOWNLOADER $LIBSSH2_URL
  tar xf libssh2-*
  cd libssh2-*/
  ./configure --prefix=$PREFIX_LIBS --host=$TARGET_HOST --with-libssl-prefix=$PREFIX_LIBS --without-libgcrypt --with-openssl --without-wincng --enable-static --disable-shared
  make $NUM_THREAD
  make install
}

build_libuv() {
  cd $BUILD_DIRECTORY
  rm -rf libuv-*
  $DOWNLOADER $LIBUV_URL
  tar xf libuv-*
  cd libuv-*/
  ./autogen.sh
  ./configure --prefix=$PREFIX_LIBS --host=$TARGET_HOST --enable-static --disable-shared
  make $NUM_THREAD
  make install
}

build_aria2() {
  cd $BUILD_DIRECTORY
  git clone https://gitlab.com/mitsui/e9155ce4.git
  cd e9155ce4
  if [ -n "$UNLOCK_THREAD_LIMIT" ]; then
    ARIA2_SRC_PATH=`pwd` $HOME/aria2_thread_patch.sh
  fi
  autoreconf -i
  ./configure --prefix=$ARIA2_PREFIX --host=$TARGET_HOST --enable-shared=no \
    --with-openssl --with-libssh2 --with-sqlite3 $LIBUV_OPTION \
    --with-ca-bundle='/etc/ssl/certs/ca-certificates.crt' \
    --without-libxml2 --without-libgcrypt --without-libnettle --without-gnutls --without-libgmp \
    ARIA2_STATIC=yes
  make $NUM_THREAD
  make install
}

[ -f $PREFIX_LIBS/lib/libz.a ] || build_zlib
[ -f $PREFIX_LIBS/lib/libexpat.a ] || build_expat
[ -f $PREFIX_LIBS/lib/libcares.a ] || build_c_ares
[ -f $PREFIX_LIBS/lib/libcrypto.a -a -f $PREFIX_LIBS/lib/libssl.a ] || build_openssl
[ -f $PREFIX_LIBS/lib/libsqlite3.a ] || build_sqlite3
[ -f $PREFIX_LIBS/lib/libssh2.a ] || build_libssh2
if [ "$USE_LIBUV" = "yes" ]; then
  [ -f $PREFIX_LIBS/lib/libuv.a ] || build_libuv
  LIBUV_OPTION=--with-libuv
fi
touch $PREFIX_LIBS/DEPMARK_$DEPS_VER

build_aria2

cd $ARIA2_PREFIX/bin
$TARGET_HOST-strip -s -x *
ls -hl
mv * $HOME
